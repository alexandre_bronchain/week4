#- creates an array with 10 elements
array = [42, 22, 41, 1, 2, 3, 6, 7, 8, 9];

#- prints the elements to the screen in order they were added.
print array, "\n";

#- sort the array and print it to the screen (do not modify the orignal array)
print array.sort, "\n";

#- sort the array and print it to the screen, this time modify to save ordering.
array.sort!;
print array, "\n";

#- create an array with the following elements: 1,5,8,23,4,7,5
secondArray = [1,5,8,23,4,7,5];

#- write and if statement that checks weather the above contains the number 4 and gives an output of "I have found the number 4".
if secondArray.include? 4
    puts "I have found the number 4"
end

#- Redo the above task ,but this time, place it into a method.
def containsFour(array)
    if array.include? 4
        puts "I have found the number 4"
    end
end

containsFour(secondArray);
