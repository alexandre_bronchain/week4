class Week4_1
    def shuffleStr(string)
        string.split("").shuffle.join
    end
    
    def concat(str1, str2)
        str1 + "|" + str2
    end
    
    def eventOrOdd(str)
        str.length % 2 == 0 ? "even" : "odd"
    end
    
end

week4_1 = Week4_1.new
puts(week4_1.shuffleStr("Hello World!"));
puts(week4_1.concat("Hello","World!"));
puts(week4_1.eventOrOdd("Hello World"));
